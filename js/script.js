$(document).ready(function(){

	//show-hide menu

	$('#nav-icon3').click(function(){
		$(this).toggleClass('change');
		$('.menu').fadeIn(100).toggleClass('reveal');
		$('.row.manors').toggleClass('reveal');
		$('body.book .main-content').toggleClass('reduce');

	});
	

	//slick slider 
	
 	$(document).ready(function(){
	  $('.manor-slide-show').slick({
		slidesToShow: 1,
		dots: false,
		slidesToScroll: 1,
		autoplay: true,
		speed:500,
		autoplaySpeed: 3000,
		arrows: false,
		fade:true,
		focusOnSelect: true,
		swipe:false,
		pauseOnHover:false,
		pauseOnFocus:false,
		accessibility: false
	  });
	}); 


	//show-hide menu

	$('.promotion-close').click(function(){
		$('.promotion').addClass('remove');
	});
	
		
});
